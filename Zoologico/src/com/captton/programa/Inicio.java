package com.captton.programa;

import com.captton.zoologico.Pajaros;
import com.captton.zoologico.Peces;
import com.captton.zoologico.Perros;

public class Inicio {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
		Perros pr1 = new Perros("Teki",5,15);
		
		System.out.println("Perro, su nombre es: "+pr1.getNombre());
		System.out.println(pr1.comer(5));
		System.out.println(pr1.Ladrar());

		System.out.println(pr1.getEdad());
		System.out.println("-------------------------");
		
		Peces pe1 = new Peces("Nemo",1,1);
		
		System.out.println("Pez, su nombre es: "+pe1.getNombre());
		System.out.println(pe1.comer(5));
		System.out.println(pe1.Nadar());
		
		System.out.println(pe1.getEdad());
		System.out.println("-------------------------");
		
        Pajaros pj1 = new Pajaros("x",1,1);
		
		System.out.println("Pajaro, su nombre es: "+pj1.getNombre());
		System.out.println(pj1.comer(5));
		System.out.println(pj1.Volar());
		
		System.out.println(pj1.getEdad());
		
	}

}
