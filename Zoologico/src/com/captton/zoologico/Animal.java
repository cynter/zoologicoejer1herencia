package com.captton.zoologico;

public abstract class Animal {
	
	protected String nombre;
	protected float edad;
	protected float peso;
	
	public Animal(String nombre, float edad, float peso) {
		super();
		this.nombre = nombre;
		this.edad = edad;
		this.peso = peso;
	}

	public Animal() {
		super();
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public float getEdad() {
		return edad;
	}

	public void setEdad(float edad) {
		this.edad = edad;
	}

	public float getPeso() {
		return peso;
	}

	public void setPeso(float peso) {
		this.peso = peso;
	}
	
	//metodo abstracto
	public abstract float comer(float peso);
	
	//metodo final
	public final float rejuvenecer() {
		edad = edad - (edad * 0.1f);
		return edad;
	}


}
